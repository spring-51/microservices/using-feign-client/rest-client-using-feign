package com.example.restclientusingfeign.client;

import com.example.restclientusingfeign.dto.userclient.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(url = "https://jsonplaceholder.typicode.com", name = "UserClient")
public interface UserClient {

    @GetMapping(value = "/users")
    public List<UserResponse> getUsers();
}
