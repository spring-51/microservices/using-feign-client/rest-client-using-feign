package com.example.restclientusingfeign.client;

import com.example.restclientusingfeign.dto.democlient.Demo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(
        /*
        this name should be unique with all other clients
         */
        name = "DemoClient",
        /*
        common url part
         */
        url = "http://127.0.0.1:8081/demo")
public interface DemoClient {

    @GetMapping
    List<Demo> getAll(@RequestParam(name = "size", defaultValue = "10") Integer size);

    @GetMapping(value = "/{id}")
    Demo get(@PathVariable Long id);

    @PostMapping
    Demo create(@RequestBody Demo demo);

    @PostMapping
    Demo createWithHeader(@RequestHeader("tokenn") String tokenn, Demo demo);
}
