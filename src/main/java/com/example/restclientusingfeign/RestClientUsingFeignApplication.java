package com.example.restclientusingfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class RestClientUsingFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestClientUsingFeignApplication.class, args);
    }

}
