package com.example.restclientusingfeign.controller;

import com.example.restclientusingfeign.client.UserClient;
import com.example.restclientusingfeign.dto.userclient.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserClient userClient;

    @GetMapping
    public List<?> getUsers(){
        List<UserResponse> users = userClient.getUsers();
        return users;
    }

}
