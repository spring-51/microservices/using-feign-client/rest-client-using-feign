package com.example.restclientusingfeign.controller;

import com.example.restclientusingfeign.client.DemoClient;
import com.example.restclientusingfeign.dto.democlient.Demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private DemoClient demoClient;

    @GetMapping
    public List<Demo> getAll(@RequestParam(name = "size", defaultValue = "10") Integer size){
        return demoClient.getAll(size);
    }

    @GetMapping(value = "/{id}")
    public Demo get(@PathVariable Long id){
        return demoClient.get(id);
    }

    @PostMapping
    public Demo create(@RequestBody Demo demo){
        return demoClient.create(demo);
    }

    @PostMapping(value = "/with-header")
    public Demo createWithHeader(@RequestBody Demo demo){
        return demoClient.createWithHeader("abcd",demo);
    }
}
