# rest-client-using-feign

## Client service

```
This service's DemoController depends on other spring boot app
gitlab repo -  
name - rest-client-resource-server
https://gitlab.com/spring-51/microservices/using-feign-client/rest-client-resource-server.git
branch - master
```

### Steps
```step
1. Add following dependency in pom
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>

2. As its a cloud project add below propererty in properties
<properties>
    .
    .
    <spring-cloud.version>2021.0.0</spring-cloud.version>
    .
</properties>

Note : version should be aligned(compatible) with spring boot version
       - refer - https://spring.io/projects/spring-cloud#overview

3.add below as dependency management . Ques - WHY ?
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>${spring-cloud.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement> 

4. Add @EnableFeignClients over any configuration or bootstrap class.
5. Add any client 
   - refer - com.example.restclientusingfeign.client.DemoClient 
6 Autowire client anywhere to use it
   - refer - DemoController
```

## Imp Endpoints

```
1. POST  http://127.0.0.1:8080/demo/with-header
request body 
{
    "id": 1001,
    "name": "name of-1001"
}

- here we have passed cusm header to the client service
  we have added filter in the client service to log this
  header value

2. POST  http://127.0.0.1:8080/demo
request body 
{
    "id": 1001,
    "name": "name of-1001"
}
- same as point 1, but here we have not passed any header 
- internally both Point 1 and 2 is calling same api of client service
```